<?php
namespace Zeflasher\PhpDal\Db;
class Tools
{
	private $mysqli;
	private $dumpDelimiter = "**BREAK**";
	
	public function __construct( $msqliConnection = null )
	{
		if ( is_null( $msqliConnection ) )
        {
            throw( new \RuntimeException("You must provided a mysqli connection") );
        }
		$this->mysqli = $msqliConnection;
	}
	
/*
 * DB OPERATION
 */
	
	/*
	* Copy a mysql database to a new one
	*/
	public function copyDB( $dbName, $newDbName )
	{
	//	check if $dbName db exist
		if ($this->mysqli->select_db($dbName)===false)
		{
			// TODO: log
			print "ERROR: Can't select database $dbName to copy from. The database may not exists\nCancelling operation\n".$this->mysqli->error;
			return false;
		}
		
	//	check if $newDbNamedb already exist
		if ($this->mysqli->select_db($newDbName)!==false)
		{
			print "ERROR: A database with the name $newDbName already exist\nCancelling operation\n";
			return false;
		}
		
		print "- Copying database $dbName to $newDbName\n";
	//	everything is fine, copy the database
		if ( !$this->mysqli->query( "CREATE DATABASE ".$newDbName." DEFAULT CHARACTER SET utf8 COLLATE utf8_bin" ) ) 
		{
			print( "ERROR: Copy failed - ".$this->mysqli->error."\n" );
			return false;
		}
		
		$tables = $this->mysqli->query( "SHOW TABLES FROM ".$dbName );
		while ( $fetch = $tables->fetch_row() ) 
		{
			print "-- Copying table $fetch[0]\n";
			$this->mysqli->query("CREATE TABLE $newDbName.$fetch[0] LIKE $dbName.$fetch[0]") or die( "ERROR: Copy failed - ".$this->mysqli->error );
			$this->mysqli->query("INSERT INTO $newDbName.$fetch[0] SELECT * FROM $dbName.$fetch[0]") or die( "ERROR: Copy failed - ".$this->mysqli->error );
		}

    //  copy the triggers and stored procedure
    /*    $query = "SELECT * FROM information_schema.TRIGGERS
                  WHERE TRIGGER_SCHEMA LIKE '".$dbName."'";
        $triggers = $this->mysqli->query($query);
        if( !triggers )
            die("ERROR: can't select triggers for db $dbName in information_schema : ".$query." : ".$this->mysqli->error);
        $query = "";
        while( $trigger = $triggers->fetch_object())
        {
             $names = array();
             $values = array();
             foreach ($record as $name => $value)
             {

                if ( is_null($value) )
                    array_push($values, "NULL");
                else
                    array_push($values, "'".$this->mysqli->real_escape_string($value)."'" );
             }
            $query .= "INSERT INTO information_schema.TRIGGERS (`".implode("`,`", $names)."`) VALUES(".implode(",", $values ).")";
            if( !$this->mysqli->query($query) )
                die("ERROR: Could not insert the triggers : ".$query." : ".$this->mysqli->error);
        }*/

		return true;
	}
	
	public function dropDB( $dbName )
	{
        print("Dropping database $dbName");
		$query = "DROP DATABASE $dbName\n";
		if ( !$this->mysqli->query( $query ) )
		{
			print "ERROR: Can't drop database $dbName : $query : ". $this->mysqli->error;
			return false; 
		}
		return true;
	}

    /**
	 * Backup ( copy ) the database using provided suffix.
	 * "_backup" suffix will be used if none provided
     * @param  $dbName
     * @param string $suffix
     * @return bool
     */
	public function backupDB( $dbName, $suffix = "_backup")
	{
		$backupDb = $dbName.$suffix;
		print "- Backing up database $dbName to $backupDb\n";
		
		if ( !$this->copyDB( $dbName, $backupDb ) ) 
		{
			print "ERROR: Could no copy database from $dbName to $backupDb";
			return false;
		}
		return true;
	}

    /**
     * Rename a DB
     * @param  $oldDB
     * @param  $newDB
     * @return bool
     */
	public function renameDB( $oldDB, $newDB )
	{
		print "\nRenaming (copy to/delte old) $oldDB to $newDB\n";
		if ( $this->mysqli->select_db($newDB) )
		{
			print "ERROR: Can't rename database $oldDB to $newDB. The database $newDB already exits\n";
			return false;
		}
		
		if ( !$this->copyDB( $oldDB, $newDB ) ) return false;
		if ( !$this->dropDB( $oldDB ) ) return false;
		return true;
	}

    /**
     * Merge record from src to dest table.
     * @param string $dbSrc
     * @param string $dbDest
     * @param string $tableName
     * @param bool $keepOriginalIds
     * @param bool $failsOnDuplicate
     * @return bool
     */
    public function mergeTable($dbSrc, $dbDest, $tableName, $keepOriginalIds = true, $failsOnDuplicate = true )
    {
        print("Merging table $dbSrc.$tableName into $dbDest.$tableName\n");
        if ( !$keepOriginalIds ) die("ERROR: Sorry but the 'not keeping original IDs' hasn't been implemented yet");

    //  check if the db exists on both dbs
        $result = $this->mysqli->query( "SHOW TABLES FROM $dbSrc LIKE '$tableName';");
        if ( $result->num_rows == 0 ) die ("Database $dbSrc doesn't have a table named $tableName");

        $result = $this->mysqli->query( "SHOW TABLES FROM $dbDest LIKE '$tableName';");
        if ( $result->num_rows == 0 ) die ("Database $dbDest doesn't have a table named $tableName");

    //  check if the table definition are the same
        $srcStructure = array();
        $destStructure = array();
        $dbSrc_tblDefinition = $this->mysqli->query( "SHOW FULL COLUMNS FROM $dbSrc.$tableName" );
        $dbDest_tblDefinition = $this->mysqli->query( "SHOW FULL COLUMNS FROM $dbDest.$tableName" );

	//	put column structure in array to work with it
		while ( $column = $dbSrc_tblDefinition->fetch_object() )
		{
 			$srcStructure[$column->Field] = $column;
		}
		while ( $column = $dbDest_tblDefinition->fetch_object() )
		{
 			$destStructure[$column->Field] = $column;
		}
        foreach( $srcStructure as $columnName => $columnDefinition )
        {
            if ( $destStructure[$columnName] )
            {
            //	check if old column is primary or index and remove ( will be set again by the new one )
                if ( !$this->__deepCompare($columnDefinition, $destStructure[$columnName]) )
                    die("ERROR: The structure of $dbSrc.$tableName is different from the one of $dbDest.$tableName for the field $columnName");
            }
            else die ("ERROR: The destination database $dbDest doesn't have the field $columnName");
        }

    //  everything is sweet, merge the table records, keeping or not original IDs
        $resultRecord = $this->mysqli->query( "SELECT * FROM $dbSrc.$tableName" );
        $query = "";
        if ( $resultRecord->num_rows == 0 ) return true;
        while ( $record = $resultRecord->fetch_object() )
        {
            //This stops SQL Injection in GET vars
            $names = array();
            $values = array();
            foreach ($record as $name => $value)
            {
                if ( is_null($value) )
                    array_push($values, "NULL");
                else
                    array_push($values, "'".$this->mysqli->real_escape_string($value)."'" );

                array_push($names, $name);
            }

            if ( $keepOriginalIds )
            {
                $query .= "INSERT INTO $dbDest.$tableName (`".implode("`,`", $names)."`) VALUES(".implode(",", $values ).")";
                if (!$failsOnDuplicate) $query .= " ON DUPLICATE KEY UPDATE `".$names[0]."` = ".$values[0];
                $query .= ";";
            }
            else
            {
            //   TODO Check class.upgrade.php function __merge
            }
        }

    //  execute the multi insert query
		if ($this->mysqli->multi_query($query)) {
		    do {
		        if ($result = $this->mysqli->store_result())
		            $result->free();
		    } while ($this->mysqli->next_result());
		}

		if ($this->mysqli->errno)
  			die( "\nERROR: Can't insert row into $dbDest.$tableName : ".$query." : ". $this->mysqli->error );

        return true;
    }

    /**
     * Synchronize db structure, using the refDb
     * @param string $dbName
     * @param string $refDb
     * @param bool $drop
     * @param bool $createMissingTable
     * @return array|bool
     */
	public function syncDBs( $dbName, $refDb, $drop = false, $createMissingTable = true )
	{	
		print "\nUpdrading database $dbName to match $refDb\n";
		
	//	now alter the db to be as $refDb
	//	get all the $refDb table
	//	check information _schema for a nicer code when using Mysql 5.1
		$tablesDef = $this->mysqli->query( "SHOW TABLE STATUS FROM ".$refDb );
		$tablesCurrent = $this->mysqli->query( "SHOW TABLE STATUS FROM ".$dbName );
		
		$adefCollation = array();
		$adefEngine = array();
		$aDef = array();
		$aCurrent = array();
		
	//	loop s6def_org tables and get structure 
		while ($fetch = $tablesDef->fetch_object() ) 
		{
			$aDef[$fetch->Name] = $this->mysqli->query( "SHOW FULL COLUMNS FROM $refDb.$fetch->Name" );
			$adefCollation[$fetch->Name] = $fetch->Collation;
			$adefEngine[$fetch->Name] = $fetch->Engine;
		}
		
	//	loop org table and get structure
		while ($fetch = $tablesCurrent->fetch_object() ) 
		{
			$aCurrent[$fetch->Name] = $this->mysqli->query( "SHOW FULL COLUMNS FROM $dbName.$fetch->Name" );
		}
		
	//	loop
		foreach( $aDef as $table => $structure)
		{
		//	alter table --- HARDCORE !!!
			if( $aCurrent[$table] ) 
			{
				if( !$this->__alterTableStructure( $refDb, $dbName, $table, $adefCollation[$table], $structure, $aCurrent[$table], $adefEngine[$table] ) ) return false;
			}
		//	create table --- EASIER :)
			else if ($createMissingTable)
			{
				if ( !$this->__createTableStructure( $dbName, $table, $refDb ) ) return false;
			}
		//	remove the table from the array as processed
			unset($aCurrent[$table]);
		}
		
		if ( $drop )
		{
			foreach ( $aCurrent as $table => $structure )
			{
				if ( !$this->dropTable( $dbName, $table ) ) return false;
			}
		}
		
		print "Synchronization finished\n\n";
		
	//	return table which was left ( so in need of data migration from the dbName )
		return $aCurrent;
	}

/*
 * TABLE OPERATION
 */
	/*
	 * create a new "tableName" using $refDb as model
	 */
	private function __createTableStructure( $dbName, $tableName, $refDb )
	{
		print( "-- Create table $dbName.$tableName\n");
		if ( !$this->mysqli->query("CREATE TABLE $dbName.$tableName LIKE $refDb.$tableName") )
		{
			print ( "ERROR: Can't create table $tableName on $dbName database" );
			return false;
		}
		return true;
	}
	
	
	/*
	 * alter the table to have structure in sync
	 */
	private function __alterTableStructure( $refDb, $dbName, $tableName, $tableCollation, $newStructure, $currentStructure, $engine = "INNODB" )
	{
		print( "-- Alter table $dbName.$tableName\n");
		
		$aNewStructure = array();
		$aCurrentStructure = array();
		
	//	put column structure in array to work with it
		while ( $column = $newStructure->fetch_object() )
		{
 			$aNewStructure[$column->Field] = $column;
		}
		
		while ( $column = $currentStructure->fetch_object() )
		{
 			$aCurrentStructure[$column->Field] = $column;
		}
		
		
		$alterQuery = "";
	//	compare structure ( add / alter )
		foreach( $aNewStructure as $columnName => $columnDefinition )
		{
			if ( $aCurrentStructure[$columnName] )
			{
			//	check if old column is primary or index and remove ( will be set again by the new one )
				if ( !$this->__deepCompare($columnDefinition, $aCurrentStructure[$columnName]) )
				{
					print( "--- Alter column $dbName.$tableName.$columnName\n");
					$alterQuery .= " CHANGE ". $columnName ." ". $columnName ." ". $columnDefinition->Type ." ". ($columnDefinition->Null=="YES"?"NULL":"NOT NULL") ." ". ($columnDefinition->Extra=="auto_increment"?"AUTO_INCREMENT":"") ." ". ( $columnDefinition->Collation=="NULL"||empty($columnDefinition->Collation)?"":"COLLATE $columnDefinition->Collation" ) ." ". ($columnDefinition->Default!=""&&$columnDefinition->Type!="TIMESTAMP"?"DEFAULT ".$this->__outputByType($columnDefinition->Type, $columnDefinition->Default):"") ." ". ($columnDefinition->Comment!=""?"COMMENT \"$columnDefinition->Comment\"":"") .",";
				}			
				unset($aCurrentStructure[$columnName]);
			}
			else
			{
				$alterQuery .= " ADD ". $columnName ." ". $columnDefinition->Type ." ". ($columnDefinition->Null=="YES"?"NULL":"NOT NULL") ." ". ($columnDefinition->Extra=="auto_increment"?"AUTO_INCREMENT":"") ." ". ( $columnDefinition->Collation=="NULL"||empty($columnDefinition->Collation)?"":"COLLATE $columnDefinition->Collation" ) ." ". ($columnDefinition->Default!=""&&$columnDefinition->Type!="timestamp"?"DEFAULT ".$this->__outputByType($columnDefinition->Type, $columnDefinition->Default):"") ." ". ($columnDefinition->Comment!=""?"COMMENT \"$columnDefinition->Comment\"":"") .",";
				print( "--- Add column $dbName.$tableName.$columnName\n");		
			}			
		}
		
	//	drop old columns not used anymore
		foreach( $aCurrentStructure as $columnName => $columnDefinition )
		{
			$alterQuery .= " DROP $columnName,";
			print( "--- Drop column $dbName.$tableName.$columnName\n");
		}
		
	//	ALTER  INDEXES
		$indexesRef = $this->mysqli->query( "SHOW INDEX FROM $refDb.$tableName" );
		$indexesCurrent = $this->mysqli->query( "SHOW INDEX FROM $dbName.$tableName" );
			
	//	put indexes structure result in array to work with it
		$aIndexesRef = array();
		
	//	Drop old indexes
	//	TODO: drop indexes ( not primary ) when table is empty ( faster ) and then here just drop primary key
		$currentKey = "";
		while ( $index = $indexesCurrent->fetch_object() )
		{
			if ( $index->Key_name != $currentKey ) 
			{
				if ( $index->Key_name == "PRIMARY" ) $alterQuery .= " DROP PRIMARY KEY,";
				else $alterQuery .= " DROP INDEX $index->Key_name,";
				$currentKey = $index->Key_name;
			}
		}

	//	create new indexes
		while ( $index = $indexesRef->fetch_object() )
		{
			if( is_null($aIndexesRef[$index->Key_name]) ) $aIndexesRef[$index->Key_name] = array();	
 			$aIndexesRef[$index->Key_name][] = $index;
		}
		
		foreach( $aIndexesRef as $key => $value )
		{		
			$aKey = array();
			foreach( $value as $indexes )
			{
				$aKey[] = $indexes->Column_name;
				print( "---- Index: Add column ".$indexes->Column_name." to key ".$key."\n");
			}
			
			$sKey = implode( ',', $aKey);
			if ( $key == "PRIMARY" ) $alterQuery .= " ADD PRIMARY KEY($sKey),";
			else $alterQuery .= " ADD ".(($indexes->Non_unique==0)?"UNIQUE":"KEY"). " $key USING $indexes->Index_type ($sKey),";
		}		
		
	//	get charset and change the default charset
	   /* $query= 'SHOW CREATE TABLE ' .$refDb.".".$tableName;
	    $tables_def = $this->mysqli->query( $query ) or die('Query error: $query ' .mysql_error());
		$table_def = $tables_def->fetch_row();
		preg_match('/\b(CHARSET=)(\w*)\b/', $table_def[1], $matches);*/
		$alterQuery .= " DEFAULT COLLATE ".$tableCollation." ENGINE=".$engine;
		
		if ( $alterQuery != "" )
		{
		//	create alter query
			$alterQuery = "ALTER TABLE $dbName.$tableName". $alterQuery;//substr( $alterQuery, 0, -1 );
			if ( !$this->mysqli->query( $alterQuery ) ) 
			{
				print ( "ERROR: Can't alter table $dbName.$tableName : $alterQuery : ".$this->mysqli->error );
				return false;
			}
		}
		
		return true;
	}

    /**
     * @param $file
     * @param null $useCustomDelimiter
     * @return bool
     */
	public function createDBFromDump( $file, $useCustomDelimiter = null )
	{
		print( "\nCreating db on v3 server from $file dump\n" );
	
		if ( $useCustomDelimiter===true )
			$delimiter = $this->dumpDelimiter;
		else $delimiter = ";";
		
	//	open the file
		print( "Opening dump file $file\n" );
		if( ($fh = fopen( "./backup/$file", 'rb' )) === FALSE ) 
		{
			print( "Error opening dump file $file");
			return false;
		}

		print( "Reading dump file $file\n" );
		$fullquery = fread( $fh, filesize("./backup/$file") );
		$queries = explode( $delimiter, $fullquery );
		
		print( "Processing queries\n" );
		$l = count( $queries);
		for( $i=0; $i<$l-1; $i++)
		{
			$query = trim( $queries[$i] ); 
			if ( is_null( $query ) || empty( $query ) ) continue;
			$this->mysqli->query( $query )
		       	or die ( "ERROR: Can't query to create from dump file : $query : ".$this->mysqli->error );
		}
		print( "Closing dump file\n" );
		fclose($fh);
				
		/*$query = fread( $fh, filesize("./backup/$file") );
		fclose( $fh );
	
		if ($this->mysqli->multi_query($query)) {
		    do {
		        if ($result = $this->mysqli->store_result())
		            $result->free();
		    } while ($this->mysqli->next_result());
		}
		
		if ($this->mysqli->errno)
  			die( "\nERROR: Can't create database from dump $file : ". $this->mysqli->error ); */
		
		return true;
	}
	
	public function dumpDB( $dbName, $file = null, $dbNameDump = null, $useCustomDelimiter = null, $drop = true, $create = true, $appendToFile = false )
	{	
		if ( $useCustomDelimiter===true )
			$delimiter = $this->dumpDelimiter;
		else $delimiter = "";
		
		if ( is_null ($dbNameDump) ) $dbNameDump = $dbName;
		
		if( is_null( $file ) ) $file = $dbName."_backup.sql";
		if ( $appendToFile && file_exists($file) ) unlink( "./backup/".$file );
		
		print "- Dump database $dbName to $file\n";
	
		//	check if $dbName db exist
		if ($this->mysqli->select_db($dbName)===false)
		{
			// TODO: log
			print "ERROR: Can't select database ".$dbName." to dump. The database may not exists\nCancelling operation\n".$this->mysqli->error."\n";
			return false;
		}
		$query = "";
		if ( $drop ) $query .= "DROP DATABASE IF EXISTS ".$dbNameDump.";$delimiter\n";
		if ( $create ) $query .= "CREATE DATABASE ".$dbNameDump." DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;$delimiter\n";
		$query .= "USE ".$dbNameDump.";$delimiter\n";
				
	//	open the file and create it if not
		$type = $appendToFile?'ab':'wb';
		$fh = fopen("./backup/$file", $type);
	//	insert the query
		fwrite( $fh, $query );
		fclose( $fh );
		
		$tables = $this->mysqli->query( "SHOW TABLES FROM ".$dbName );
		while ( $fetch = $tables->fetch_row() ) 
		{
			$this->dumpTable( $dbName, $fetch[0], $file, $useCustomDelimiter, $drop, $create );
		}

		return $file;
	}
	
	public function dumpTable( $dbName, $tableName, $file = null, $useCustomDelimiter = null, $drop = true, $create = true )
	{
		if ( is_null( $file ) ) $file = $dbName."_backup.sql";
		
		if ( $useCustomDelimiter===true )
			$delimiter = $this->dumpDelimiter;
		else $delimiter = "";
		
		print( "-- Dump table $dbName.$tableName to ./backup/$file\n");
		$dumpQuery = "";
		
	//	create table structure
		if( $drop ) $dumpQuery .= "\nDROP TABLE IF EXISTS `".$tableName."`;$delimiter\n";
		
		if ($create) 
		{
			$query = "SHOW CREATE TABLE $dbName.$tableName";
			$tables = $this->mysqli->query( $query ) 
				or die( "ERROR: Could not create sql table creation for $dbName.$tableName : $query : ".$this->mysqli->error  );
			$table = $tables->fetch_row();
			$dumpQuery .= $table[1].";$delimiter\n";
		}
				
	//	Select the row of the table and create sql from it
		$query = "SELECT * FROM $dbName.$tableName";
		$rows = $this->mysqli->query( $query )
			or die( "ERROR: Can't select table for dump : $query : ". $this->mysqli->error );
			
		while( $row = $rows->fetch_row() )
		{
			foreach ( $row as &$value )
			{
				if ( is_numeric( $value ) ) continue;
				if ( is_string( $value ) ) 
				{
					$value = str_replace( array("–","’","“", "•"), array("\"","'","\"","-"), $value );
					$value = "'".$this->mysqli->real_escape_string( $value )."'"; 
				}
				else if ( is_null( $value ) ) $value = 'NULL';
			}
			$dumpQuery .= "INSERT INTO `$tableName` VALUES(".implode(",",$row).");$delimiter\n";
		}
		
	//	open the file and create it if not
		$fh = fopen("./backup/$file", 'ab');
	//	append the query
		fwrite( $fh, $dumpQuery );
		fclose( $fh );
		
	//	write to file dbName.backup.sql
		return true;
	}
	
	public function dropTable( $dbName, $tableName )
	{
		print( "-- Drop table $dbName.$tableName\n");
		$dropQuery = "DROP TABLE IF EXISTS $dbName.$tableName";
		if( !$this->mysqli->query( $dropQuery ) )
		{
			print ("ERROR: Could not drop the table sql table $dbName.$tableName : ".$this->mysqli->error );
			return false;
		}
		return true;		
	}
	
	public function getOrgDatabases()
	{
		$result = array();
		
		// get a list of all the databases
		$sql = "SHOW DATABASES";
		if ($dbResult = $this->mysqli->query($sql))
		{
			// for each database
			while ($obj = $dbResult->fetch_object())
			{
				// see if there is a modules table
				$sql = "SHOW TABLES FROM ".$obj->Database." LIKE '%s_modules%'";
				if ($dbTable = $this->mysqli->query($sql))
				{
					// set the database we are working on
					$this->mysqli->select_db($obj->Database);
					
					// check to see if it is an org database
					$sql = "SELECT * FROM s_modules WHERE module = 'dbType' AND parameters = 'org'";
					$modulesResult = $this->mysqli->query($sql);
					if ($modulesResult->num_rows >0 )
					{
						array_push($result, $obj->Database);
					}
				}
			}
		}
		
		return $result;
	}

/* FIELD FUNCTIONS */
	/*
	 * New values and old Values are matching array,
	 * eg the script is looking for the oldvalue index 0 and will replace it by the new value index 2
	 *
	 * dbName String name of the db to work on
	 * fieldName String|Array list of field name where the value must be updated
	 * value Array new values to be update
	 * oldValue Array old value to do the update from. Pass null to replace any value for this field
	 */
	public function updateFieldValue( $dbName, $fieldName, $value, $oldValue = null )
	{
		print("DB $dbName, updating field $fieldName value ".implode(",",$oldValue)." to ".implode(",", $value)."\n");

	//	show table definition
		$tables = $this->mysqli->query( "SHOW TABLES FROM ".$dbName );

	//	check each tables
		while ( $fetch = $tables->fetch_row() )
		{
			$query = "";
			$table = $fetch[0];
		//	get the table structure
			$structure = $this->mysqli->query( "DESCRIBE $dbName.$table" ) or die ($this->mysqli->error);
		//	check each field
			while( $field = $structure->fetch_object() )
			{
			//	if the field name we gave is in a string format ( not array of fieldnames )
				if ( is_string($fieldName) )
				{
				//	if we have a matching field on the table
					if ( $field->Field == $fieldName )
					{
					//	have we sent an array of values and oldvalues
						if( is_array( $value ) && ( is_null($oldValue) || is_array( $oldValue) ) )
						{
						//	if count different warn the user that something went wrong
							if( !is_null($oldValue) && count($value) != count($oldValue) )
							{
								print( "ERROR: new values and old values array doesn't have the same length");
								exit();
							}
						//	Loop in each value and create the update query
							for ($i=0; $i<count($value); ++$i )
							{
								$query .= "UPDATE $dbName.$table SET $fieldName = ". $this->mysqli->real_escape_string($value[$i]);
								if ( !is_null( $oldValue) ) $query .= " WHERE $fieldName = ". $oldValue[$i];
								$query .= ";";
							}
						}
						else
						{
						//	if we haev passed an array for oldValue, warn the user somethign went wrong
							if ( is_array( $oldValue ) )
							{
								print( "ERROR: you can't passed an array for old values if new value is a string" );
								exit();
							}

							$query .= "UPDATE $dbName.$table SET $fieldName = ". $this->mysqli->real_escape_string($value);
							if ( !is_null( $oldValue) ) $query .= " WHERE $fieldName = $oldValue";
							$query .= ";";
						}
					}
				}
				else
				{
					$count = count( $fieldName );
					for ( $i=0; $i<$count; $i++ )
					{
						if ( $field->Field == $fieldName[$i] )
						{
						//	have we sent an array of values and oldvalues
							if( is_array( $value ) && ( is_null($oldValue) || is_array( $oldValue) ) )
							{
							//	if coutn different warn the user that something went wrong
								if( !is_null($oldValue) && count($value) != count($oldValue) )
								{
									print( "ERROR: new values and old values array doesn't have the same length");
									exit();
								}
							//	Loop in each value and create the update query
								for ($j=0; $j<count($value); ++$j )
								{
									$query .= "UPDATE $dbName.$table SET $fieldName[$i] = ". $this->mysqli->real_escape_string($value[$j]);
									if ( !is_null( $oldValue) ) $query .= " WHERE $fieldName[$i] = $oldValue[$j]";
									$query .= ";";
								}

							}
							else
							{
							//	if we haev passed an array for oldValue, warn the user somethign went wrong
								if ( is_array( $oldValue ) )
								{
									print( "ERROR: you can't passed an array for old values if new value is a string" );
									exit();
								}

								$query .= "UPDATE $dbName.$table SET $fieldName[$i] = ". $this->mysqli->real_escape_string($value);
								if ( !is_null( $oldValue) ) $query .= " WHERE $fieldName[$i] = $oldValue";
								$query .= ";";
							}
						}
					}
				}
			}

			if ( $query != "" )
			{
				if ($this->mysqli->multi_query($query)) {
				    do {
				        if ($result = $this->mysqli->store_result())
				            $result->free();
				    } while ($this->mysqli->next_result());
				}
				if ($this->mysqli->errno)
			  		die( "\nERROR: Can't update field : ".$query." : ". $this->mysqli->error );
			}

		}
	}

    public function removeRecord( $dbName, $fieldName, $value )
    {
        $valueString = !is_array($value) ? $value : implode(",",$value);
		print("DB $dbName, removing records having field $fieldName, value $valueString\n");

	//	show table definition
		$tables = $this->mysqli->query( "SHOW TABLES FROM ".$dbName );

	//	check each tables
		while ( $fetch = $tables->fetch_row() )
		{
			$query = "";
			$table = $fetch[0];
		//	get the table structure
			$structure = $this->mysqli->query( "DESCRIBE $dbName.$table" ) or die ($this->mysqli->error);
		//	check each field
			while( $field = $structure->fetch_object() )
			{
			//	if the field name we gave is in a string format ( not array of fieldnames )
				if ( is_string($fieldName) )
				{
				//	if we have a matching field on the table
					if ( $field->Field == $fieldName )
					{
					//	have we sent an array of values and oldvalues
						if( is_array( $value ) )
						{
						//	Loop in each value and create the update query
							for ($i=0; $i<count($value); ++$i )
							{
								$query .= "DELETE FROM $dbName.$table WHERE $fieldName = '". $this->mysqli->real_escape_string($value[$i])."';";
							}
						}
						else
						{
							$query .= "DELETE FROM $dbName.$table WHERE $fieldName = '". $this->mysqli->real_escape_string($value)."';";
						}
					}
				}
				else
				{
					$count = count( $fieldName );
					for ( $i=0; $i<$count; $i++ )
					{
						if ( $field->Field == $fieldName[$i] )
						{
						//	have we sent an array of values and oldvalues
							if( is_array( $value ) )
							{
							//	Loop in each value and create the update query
								for ($j=0; $j<count($value); ++$j )
								{
                                    $query .= "DELETE FROM $dbName.$table WHERE $fieldName[$i] = '". $this->mysqli->real_escape_string($value[$j])."';";
								}
							}
							else
							{
                                $query .= "DELETE FROM $dbName.$table WHERE $fieldName[$i] = '". $this->mysqli->real_escape_string($value)."';";
							}
						}
					}
				}
			}

			if ( $query != "" )
			{
				if ($this->mysqli->multi_query($query)) {
				    do {
				        if ($result = $this->mysqli->store_result())
				            $result->free();
				    } while ($this->mysqli->next_result());
				}
				if ($this->mysqli->errno)
			  		die( "\nERROR: Can't remove field : ".$query." : ". $this->mysqli->error );
			}

		}
    }


//	compare objects	
	private function __deepCompare($a,$b) {
		if(is_object($a) && is_object($b)) {
			if(get_class($a)!=get_class($b))
		    	return false;
			foreach($a as $key => $val) {
		    	if(!$this->__deepCompare($val,$b->$key))
		      		return false;
		  	}
		  	return true;
		}
		else if(is_array($a) && is_array($b)) {
		  	while(!is_null(key($a)) && !is_null(key($b))) {
		    	if (key($a)!==key($b) || !$this->__deepCompare(current($a),current($b)))
		      		return false;
		    	next($a); next($b);
		  	}
		  	return is_null(key($a)) && is_null(key($b));
		}
		else
		  return $a===$b;
	}
	
	private function __outputByType( $type, $value )
	{
		switch ( true )
		{
			case preg_match("/char|text/i", $type ):
			 	$output = "'".addslashes($value)."'";
			break;
			case preg_match("/timestamp/i", $type ):
			//	check if we have an on update clause
				$output = "CURRENT_TIMESTAMP";
			break;
			default: 
				$output = $value;
		}
		return $output;
	}
	
/*	
 	DOESN'T WORK. CAN'T FIND INFORMATION_SCHEMA
	private function __tableExists($tablename, $database) {
	
	    $res = $this->mysqli->query("
	        SELECT COUNT(*) AS count 
	        FROM information_schema.tables 
	        WHERE table_schema = '$database' 
	        AND table_name = '$tablename'
	    ");
	 
	    return $res->num_rows == 1;
	}
*/	
	
}
?>