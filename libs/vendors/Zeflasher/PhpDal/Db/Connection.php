<?php
    namespace Zeflasher\PhpDal\Db;

    class Connection
    {
        private $__server;
        private $__log;
        private $__pwd;
        private $__db;

        public function __construct( $server, $log, $pwd, $db )
        {
            $this->__server = $server;
            $this->__log = $log;
            $this->__pwd = $pwd;
            $this->__db = $db;
        }

        public function getServer() { $this->__server; }
        public function setServer( $s ) { $this->__server = $s; }

        public function getLog() { $this->__log; }
        public function setLog( $s ) { $this->__log = $s; }

        public function getPwd() { $this->__pwd; }
        public function setPwd( $s ) { $this->__pwd = $s; }

        public function getDB() { $this->__db; }
        public function setDB( $s ) { $this->__db = $s; }

        public function __toString() {
            $s  = "server: " . $this->__server;
            $s .= ", log: " . $this->__log;
            $s .= ", pwd: " . $this->__pwd;
            $s .= ", db: " . $this->__db;
            return $s;
        }
    }

?>