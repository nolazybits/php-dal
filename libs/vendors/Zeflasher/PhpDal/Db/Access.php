<?php
namespace Zeflasher\PhpDal\Db;

class Access
{
    /******************************************************************************
     *	VARIABLES
     ******************************************************************************/

    // associative array holding the key and mysqli
    static private $__aMapMysqli;

    /******************************************************************************
     *	PUBLIC FUNCTIONS
     ******************************************************************************/

    /**
     * Gets the MySQLi object depending of the ID passed
     * @static
     * @param string $type (default "Root")
     * @return bool|\mysqli
     */
    static public function get( $type = \Zeflasher\PhpDal\Db\DBDefault::TYPE )
    {
        // Create the HashMap holding the dbName / Mysqli object pair
        if ( !isset( Access::$__aMapMysqli ) ) Access::$__aMapMysqli = array();

        //  if the connection doesn't exist, create it
        if(array_key_exists($type, Access::$__aMapMysqli) )
        {
            //  get the connection from the HasMap
            $conn = Access::$__aMapMysqli[$type];
        }
        else
        {
            $conn = Access::__addConnection( $type );
        }
        //  return the connection object
        return $conn;
    }

    /**
     * executes a query
     * @param string $sql
     * @param string $type
     * @throws \RuntimeException
     * @return \MySQLi_Result | bool False on failure.<br/>
     * For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries <b>mysqli_query</b> will return
     * a <b>MySQLi_Result</b> object.<br/>
     * For other successful queries <b>mysqli_query</b> will return true.
     */
    static public function query( $sql = "", $type = \Zeflasher\PhpDal\Db\DBDefault::TYPE )
    {
        //  get connector
        $conn = Access::get($type);
        if ($conn === false)
        {
            throw new \RuntimeException("Unable to connect to database: ".mysqli_connect_error());
        }

        $conn->set_charset('utf8');

        //  execute the query
        $rs = $conn->query($sql);

        //  if the query succeed
        if ($conn->error == "")
        {
            return $rs;
        }
        else
        {
            throw new \RuntimeException("Problem performing your db request- ".$conn->error . " - " .$sql);
            // - ".$conn->error . " - " .$sql
        }
    }

    /**
     * executes a multi query, Mainly use to set variables, ...
     * @param string $sql
     * @param string $type
     * @throws \RuntimeException
     * @return \MySQLi_Result | bool False on failure.<br/>
     * For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries <b>mysqli_query</b> will return
     * a <b>MySQLi_Result</b> object.<br/>
     * For other successful queries <b>mysqli_query</b> will return true.
     */
    static public function multi_query( $sql = "", $type = \Zeflasher\PhpDal\Db\DBDefault::TYPE )
    {
        //  get connector
        $conn = Access::get($type);
        if ($conn === false)
        {
            throw new \RuntimeException("Unable to connect to database: ".mysqli_connect_error());
        }

        $conn->set_charset('utf8');

        //  execute the query
        $rs = $conn->multi_query($sql);

        while ($conn->more_results() && $conn->next_result());

        //  if the query succeed
        if ($conn->error == "")
        {
            return $rs;
        }
        else
        {
            throw new \RuntimeException("Problem performing your db request- ".$conn->error . " - " .$sql);
            // - ".$conn->error . " - " .$sql
        }
    }

    /**
     * Return the last insert_id
     * @static
     * @param string $sql
     * @param string $type
     * @return \MySQLi_Result|bool False on failure.<br/>
     * For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries <b>mysqli_query</b> will return
     * a <b>MySQLi_Result</b> object.<br/>
     * For other successful queries <b>mysqli_query</b> will return true.
     */
    static public function queryInsertion( $sql = "", $type = \Zeflasher\PhpDal\Db\DBDefault::TYPE )
    {
        static::query($sql, $type);
        return Access::get($type)->insert_id;
    }

    /**
     * sanitises string data to prevent sql injection
     * @static
     * @param string $string
     * @return string an escaped, sql injection safe, string
     */
    public static function sanitise( $string = "" )
    {
        //  handle magic quotes if they exist
        if(get_magic_quotes_gpc()) $string = stripslashes($string);

        //  escape to prevent sql injection
        $safeString = "";

        if( Access::get() )
        {
            $safeString = Access::get()->real_escape_string($string);
        }
        return $safeString;
    }

    /**
     * sanitises a sql query string and arguments (as optional parameters), to prevent sql injection
     * @static
     * @param string $sql
     * @return mixed
     */
    public static function sanitiseQuery( $sql = "" )
    {
        //  get the additional arguments sent to this function
        $args = func_get_args();
        foreach($args as $key => $val)
        {
            $args[$key] = Access::sanitise($val);
        }
        $args[0] = $sql;

        //  return a string produced according to the formatting string format in the raw sql.
        return call_user_func_array('sprintf', $args);
    }

    /**
     * close all open mysqli connections
     */
    public static function closeConnections()
    {
        //  Create the HashMap holding the dbName / MySQLi object pair
        if ( !isset( Access::$__aMapMysqli ) )
        {
            Access::$__aMapMysqli = array();
        }
		
		foreach (Access::$__aMapMysqli as $key => $value)
		{
			Access::$__aMapMysqli[$key]->close();
		}
		Access::$__aMapMysqli = array();
	}

	/******************************************************************************
	*	PRIVATE FUNCTIONS
	******************************************************************************/
    /**
     * adds a connection to the has map
     * @static
     * @param string $key the database connection key
     * @return bool|\mysqli false on failure or a mysqli resource on success
     */
    private static function __addConnection( $key )
    {
        //  create the mysqli object
        $className = sprintf("\\Zeflasher\\PhpDal\\Db\\%s", $key);

        $mysqli = new \mysqli( $className::SERVER, $className::USERNAME, $className::PASSWORD, $className::DATABASE, $className::PORT);

        //  if it failed return false
        if ($mysqli->connect_error)
        {
            return false;
        }

        // Many new attack vectors rely on encoding bypassing. Use UTF-8 as your database and application charset
        // unless you have a mandatory requirement to use another encoding.
        $mysqli->set_charset('UTF-8');

	//  otherwise add the connection in our array and return the connection
		Access::$__aMapMysqli[$key] = $mysqli;
		return $mysqli;
	}
}
?>
