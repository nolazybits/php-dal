<?php
namespace Zeflasher\PhpDal\Db;
/**
 * User: zeflasher
 * Date: 10/07/12
 * Time: 10:31 PM
 * using this template, make config files for your specific database requirements
 */
final class AConfig
{
    const TYPE = "";

    const SERVER =  "localhost";
    const DATABASE =  "";
    const USERNAME =  "";
    const PASSWORD =  "";
}
