<?php
namespace Zeflasher\PhpDal\Db;
class ObjectPack
    implements \IteratorAggregate
{
    const PROPERTY_VALID = 0;
    const PROPERTY_INVALID = 1;

    const INDEX_PROPERTY_LOCAL = 0;
    const INDEX_PROPERTY_VALIDATION = 1;
    const INDEX_PROPERTY_REMOTE = 2;

/* ****************************************************************************
*	VARIABLES
**************************************************************************** */
    /**
     * Holds all the properties type for type checking at runtime
     * @var array
     */
    protected $_propertiesType = array();

    /**
     * Properties with the data tag in their comment will be stored in this array and _set _get will access to them
     * @var array
     */
    protected $_data;

    /**
     *
     */
    public function __construct()
    {
    //  get any public properties, push the key/value (if any) into the $_data array
        $reflect = new \ReflectionClass($this);

    //  get all properties
        $properties   = $reflect->getProperties();

    //  add them to _data if comment @data set
        foreach ($properties as $property)
        {
        //  retrieving from the comments if it belongs to the datapack _data
        //  and if so retrieve the type
            $docComment = $property->getDocComment();
            $name = $property->getName();

            if( preg_match('/@data/', $docComment, $matches) )
            {
                if(preg_match('/@var[\s]+([\.\w]+)/', $docComment, $matches))
                {
                    $this->_propertiesType[$name] = $matches[1];
                }

            //  add the property to the $_data array and unset it, if public
                $this->__createProperty($name, $this->{$name});
            }
        }

    //  post construction initialisation
        $this->_init();
    }

    /**
     * To be override by the subclasses if need to instantiate things at startup
     * @return void
     */
    protected function _init() {}

    /**
     * Get the class type
     * @static
     * @return mixed The class type
     */
    protected static function _getClass()
    {
        return __CLASS__;
    }

/* ****************************************************************************
*	PUBLIC FUNCTIONS
**************************************************************************** */
    /**
     * Clear totally the object properties
     */
    public function clear()
    {
        foreach ( $this->_data as $key => $value)
        {
            $this->_data[$key] = array(NULL, DataPack::PROPERTY_VALID, NULL);
        }
    }

/* ****************************************************************************
*	READ / WRITE PROPERTIES
**************************************************************************** */
    /**                                       $data
     * Get the property of the object
     * @param \string $property
     * @return mixed null if the property is not found, otherwise the value of the property
     */
    public function &__get( $property )
    {
        if( isset($this->_data[$property]) )
        {
            return $this->_data[ $property ][ self::INDEX_PROPERTY_LOCAL ];
        }
        return null;
    }

    /**
     * Set the property of the object and  invalidate it
     * @param string $property
     * @param mixed $value
     * @return void
     */
    public function __set( $property , $value )
    {
        if( isset($this->_data[$property]) )
        {
            $this->_updateProperty( $property, $value );
        }
    }

    /**
     * Check if the object has the property
     * @param string $property
     * @return boolean
     */
    public function __isset( $property )
    {
        return isset($this->_data[ $property ]) && isset( $this->_data[ $property ][DataPack::INDEX_PROPERTY_LOCAL] );
    }

    /**
     * The unset function DOES NOT UNSET the property, as we need to know what properties has been created by the subclass
     * This function just set the property to NULL.
     * @param string $property
     * @return void
     */
    public function __unset( $property )
    {
        $this->_data[ $property ][DataPack::INDEX_PROPERTY_LOCAL] = NULL;
    }

    /**
     * Used by the constructor to create the properties on the DataPack
     *
     * @param string $property
     * @param mixed $value
     */
    private function __createProperty( $property, $value )
    {
        // if the value is not null then the property will be invalid
        // happens when setting defaults on the class instance
        // happens when casting
        if (!is_null($value) )
        {
            $value = $this->_enforceType($property, $value);
            $this->_data[ $property ] = array( $value, self::PROPERTY_INVALID, null );
        }
        else
        {
            // if the value is null then the property is null and valid
            $this->_data[ $property ] = array( null, self::PROPERTY_VALID, null );
        }

        unset($this->{$property});
    }

    /**
     * @param string $property
     * @param mixed $value
     */
    protected function _loadProperty( $property, $value )
    {
    //	if the property exist, update it
        if ( isset( $this->_data[$property] ) )
        {
        //	if the property needs to be sent to the server, and the value received from the server is different from the local one -> CONFLICT
            if ( !$this->_isValid( $property ) && $this->_data[ $property ] != $value )
            {
            //	CONFLICT!!
            //  FOR NOW WE ARE OVERRIDING IT
                $this->_data[ $property ][self::INDEX_PROPERTY_REMOTE] = $value;
                $this->_updateProperty( $property, $value );
                $this->_validate( $property );
            }
            else
        //	update only valid properties
            if ( $this->_isValid( $property ) && $this->_data[ $property ][self::INDEX_PROPERTY_LOCAL] !== $value )
            {
                $this->_data[ $property ][self::INDEX_PROPERTY_REMOTE] = $value;
                $this->_updateProperty( $property, $value );
                $this->_validate( $property );
            }
        }
    }

    /**
     * Update the property.
     * Will throw an exception if the property is not set or the value to be set can't be cast to the property type
     *
     * @param string $property
     * @param mixed $value
     * @throws \RuntimeException
     */
    protected function _updateProperty( $property, $value )
    {
        if(!isset($this->_data[$property]))
        {
            throw new \RuntimeException(static::_getClass()."::".$property." is not set.");
        }
        else
        {
            $value = $this->_enforceType($property, $value);

        //  all good same type or typed
            if ( $this->__diffCheck($property, $value) )
            {
                //	set the property and invalidate it
                $this->_data[ $property ][self::INDEX_PROPERTY_LOCAL] = $value;
                $this->_invalidate( $property );
            }
        }
    }

    /**
     * strong types incoming values to be what is defined by the property's(@var) doc comment
     *
     * @param $property
     * @param $value
     * @return mixed
     * @throws \RuntimeException
     */
    protected function _enforceType($property, $value)
    {
        $supported_types = ["boolean","integer","float","string","array","null","utc_timestamp","geom_point"]; // "object" removed on purpose

        // if it is not a supported property type, just return it as is
        // TODO, strong type as a classes if required
        if (!in_array($this->_propertiesType[$property],$supported_types))
        {
            return $value;
        }

        if(gettype($value) != $this->_propertiesType[$property])
        {
            switch($this->_propertiesType[$property])
            {
                case "utc_timestamp":
                    settype($value, "integer");
                break;

                case "geom_point":
                    settype($value, "string");
                break;

                default:
                    if( !settype($value, $this->_propertiesType[$property]) )
                    {
                        //  Runtime type check error
                        throw new \RuntimeException(static::_getClass()."::".$property." is of type ".
                            $this->_propertiesType[$property]." ".gettype($value)." given.");
                    }
                break;
            }
        }
        return $value;
    }

    /**
     * Generate an array of changed properties
     * @return array An array of changed properties
     */
    public function getChangedProperties()
    {
        $a = array();
        foreach( $this->_data as $property => $value)
        {
            if ( !$this->_isValid( $property ) ) $a[ $property ] = $value;
        }
        return $a;
    }

    /**
     * Get an iterator for this class
     * @return \ArrayObject|\Traversable that can be iterate
     */
    public function getIterator()
    {
        $a = array();
        foreach( $this->_data as $property => $value )
        {
            $a[ $property ] = $value;
        }
        return new \ArrayObject( $a );
    }

    /**
     * Parse the associative array, set the value for the relevant properties and invalidate it
     * @param array $data
     * @return void
     */
    final public function write( $data )
    {
        foreach( $data as $property => $value )
        {
        //	set the property and invalidate it
            $this->_updateProperty( $property, $value );
        }
    }

    /**
     * Return an stdClass of all the properties for this DataPack
     * @return \stdClass
     */
    final public function read()
    {
        $a = new \stdClass();
        foreach( $this->_data as $property => $value )
        {
            $a->$property = $value[self::INDEX_PROPERTY_LOCAL];
        }
        return $a;
    }

    /**??
     * Do the diff of the old and new data and returns an array of data to be inserted / updated
     * @return <array> Associative array with the new data to be updated / inserted
     * @param $oldData Array
     * @param $newData Array
     */
    /*	public function diff( array $newData )
    {
        $diff = array();
        foreach( $newData as $property => $value )
        {
            if ( $this->__diffCheck( $property, $value) )
            {
                $diff[ $property ] = $value;
            }
        }
        return $diff;
    }*/


    /**
     * Check data object to see if any properties has changed
     * @return bool <Boolean>
     */
    public function hasChanged()
    {
        foreach( $this->_data as $property )
        {
            if ( !$this->_isValid( $property ) ) return true;
        }
        return false;
    }

   /**
    * Check if we have any pending properties
    * @param	id
    * @return
    */
    /*	public function isPending( id : uint ) : Boolean
       {
           for ( var property : String in _data )
           {
               if ( _data[ property ][1] > 1 ) return true;
           }
           return false;
       }




   /**
    * Return an array of pending properties
    * @param	id
    * @return
    */
    /*	public function getPendingProperties( id : uint ) : Array
       {
           var a : Array = new Array();
           for ( var property : String in _data )
           {
               if ( _data[ property][1] == id ) a.push( property );
           }
           return a;
       }

   /**
    * Used by subclass to load properties received from the server
    * @param	aProperties <Array> associative array received from the server
    */
    /*	public function loadProperties( aProperties : Array ) : void
       {
           for ( var property : String in aProperties )
           {
               _loadProperty( property, aProperties[property] );
           }
       }

   /**
    * Return a string representation of the DataPack
    * @return <String>
    */
    /*	public function toString() : String
    {
        return "hello";
        //var s : String = this +" :: ";
        //for ( var property : String in _data )
        //{
//trace( property );
//break;
            //s.concat( property );
        //}
        //return s;
    }*/

    /**
     * Flags the properties has "unchanged"
     */
    protected function _validateAll()
    {
        foreach( $this->_data as $property => $value )
        {
            $this->_validate( $property );
        }
    }

    /**
     * Flag the property has "unchanged"
     * @protected
     * @param	$property
     * @return	boolean false if the property doesn't exist, else true
     */
    protected function _validate( $property )
    {
        $this->_updateValidState( $property, self::PROPERTY_VALID );
    }

    /**
     * Flag this property as "changed"
     * @param   string $property
     * @return	boolean false if the property doesn't exist, else true
     */
    protected function _invalidate( $property )
    {
        $this->_updateValidState( $property, self::PROPERTY_INVALID );
    }

    /**OK
     * Update the valid state of the property
     * @param string $property
     * @param integer $state
     * @return void
     */
    protected function _updateValidState( $property, $state )
    {
        if ( isset( $this->_data[ $property ] ) ) $this->_data[ $property ][self::INDEX_PROPERTY_VALIDATION] = $state;
    }

    /**
     * TODO check this function. Seems dodgy
     * Indicate wheter the property is valid ( true ) or invalide ( false )
     * Valid property means that since the last update the property hasn't been updated locally
     * Invalid property means that since the last update the property has been updated locally
     * @protected
     * @param string $property the string of your property
     * @return boolean
     */
    protected function _isValid( $property )
    {
    //	if the property has been update or if a change is pending ( needs DB update )
        if ( $this->_data[ $property ][self::INDEX_PROPERTY_VALIDATION] == self::PROPERTY_VALID || $this->_data[ $property ][self::INDEX_PROPERTY_VALIDATION] > self::PROPERTY_INVALID ) return true;
        else return false;
    }


/* ****************************************************************************
*	PRIVATE FUNCTIONS
**************************************************************************** */
    /**
     * @private	 Check for difference betwen a value hold by a property and a value passed
     * @return 	boolean true if difference found, false otherwise
     * @param 	string $property
     * @param 	mixed $value
     */
    private function __diffCheck( $property, $value )
    {
    //	if the property is not set skip it ( as ALL properties should be set in the _data array )
        if ( !isset( $this->_data[ $property ] ) ) return false;
        return !( $this->_data[ $property ][self::INDEX_PROPERTY_LOCAL] === $value );
    }

    /**
     * Returns a string of invalidated changes with the [from]->[to] values
     * @return string
     */
    public function getStringOfChanges()
    {
        $changes = array();
        foreach( $this->_data as $property => $value )
        {
            if ( $value[self::INDEX_PROPERTY_LOCAL] != $value[self::INDEX_PROPERTY_REMOTE] )
            {
                array_push($changes, "$property [".$value[self::INDEX_PROPERTY_REMOTE]."]->[".$value[self::INDEX_PROPERTY_LOCAL]."]");
            }
        }
        $changes_str = implode(", ", $changes);
        return $changes_str;
    }

    /**
     *
     */
    public function __toString()
    {
        $changes = new \stdClass();
        foreach( $this->_data as $property => $value )
        {
            $changes->$property = $value[self::INDEX_PROPERTY_LOCAL];
        }
        $changes_str = json_encode($changes, JSON_PRETTY_PRINT);
        return $changes_str;
    }
    /**/
}