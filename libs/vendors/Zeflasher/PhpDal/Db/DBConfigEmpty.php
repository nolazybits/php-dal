<?php
namespace Zeflasher\PhpDal\Db;
/**
 * User: zeflasher
 * Date: 10/07/12
 * Time: 10:35 PM
 */
final class DBConfigEmpty
{
    const TYPE = "DBConfigEmpty";

    const SERVER =  "";
    const DATABASE =  "";
    const USERNAME =  "";
    const PASSWORD =  "";
}
