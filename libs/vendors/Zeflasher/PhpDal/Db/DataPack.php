<?php
namespace Zeflasher\PhpDal\Db;

abstract class DataPack
    extends ObjectPack
{
    /**
     * Holds all the mandatory property (name is the key), for check on insert
     * or to be invoke using the hasAllMandatoryInsertProperties
     * @see hasAllMandatoryInsertProperties
     * @var array
     */
    protected $_mandatoryInsertProperties = array();

    /**
     * Holds all the mandatory property (name is the key), for check on update
     * or to be invoke using the hasAllMandatoryInsertProperties
     * @see hasAllMandatoryUpdateProperties
     * @var array
     */
    protected $_primaryKeyProperties = array();

    /**
     * @var string
     */
    protected $_database_type;

    /**
     * @var string
     */
    protected $_table;

    /**
     *
     */
    public function __construct()
    {
        $reflect = new \ReflectionClass($this);
        $properties = $reflect->getProperties();

        foreach ($properties as $property)
        {
            $docComment = $property->getDocComment();
            $name = $property->getName();

            if (preg_match('/@primaryKey/', $docComment, $matches))
            {
                array_push($this->_primaryKeyProperties, $name);
            }

            if (preg_match('/@insert/', $docComment, $matches))
            {
                array_push($this->_mandatoryInsertProperties, $name);
            }
        }

        parent::__construct();
    }

    /**
     * Check if the object has all the mandatory insert properties set. If not an exception will be thrown
     * @return bool
     * @throws \RuntimeException
     */
    public function hasAllMandatoryInsertProperties()
    {
        $missing_properties = array();
        foreach ($this->_mandatoryInsertProperties as $name)
        {
            if (!isset($this->{$name}))
            {
                array_push($missing_properties, $name);
            }
        }
        if (count($missing_properties) > 0)
        {
            throw new \RuntimeException("The property " . join(', ', $missing_properties) . " are mandatory for insertion");
        }
        return true;
    }

    /**
     * Check if the object has all the mandatory update properties set.
     * @throws \RuntimeException
     */
    public function hasAllPrimaryKeyProperties()
    {
        $missingProperties = array();
        foreach ($this->_primaryKeyProperties as $name)
        {
            if (!isset($this->{$name}))
            {
                array_push($missingProperties, $name);
            }
        }
        if (count($missingProperties) > 0)
        {
            throw new \RuntimeException(
                "The property " . join(', ', $missingProperties) .
                    " are primary keys and needed for update if no where clause passed."
            );
        }
    }

    /* ****************************************************************************
    *	ABSTRACT METHODS
    **************************************************************************** */

    public function getDatabaseType()
    {
        return $this->_database_type;
    }

    public function getTableName()
    {
        return $this->_table;
    }

    /* ****************************************************************************
    *	DATABASE RELATED PUBLIC FUNCTIONS
    **************************************************************************** */

    /**
     * Load DB record into the DataPack
     * @param array $fields the properties/column name to be loaded.
     * If none provided (null) all the properties marked as data will be loaded
     * @param string $extra_clause put the order by, desc, ... here
     * @throws \RuntimeException
     */
    public function load($fields = null, $extra_clause = null)
    {
        //  create selection clause
        $select_clause = array();
        if (!is_null($fields))
        {
            $fieldsCount = count($fields);
            for ($i = 0; $i < $fieldsCount; $i++)
            {
                $key = $fields[$i];
                if (isset($this->_data[$key]))
                {
                    switch($this->_propertiesType[$key])
                    {
                        case "utc_timestamp":
                            array_push($select_clause, "UNIX_TIMESTAMP(" . \Zeflasher\PhpDal\Db\Access::sanitiseQuery($key) . ") AS `" . $key."`");
                        break;

                        case "geom_point":
                            array_push($select_clause, "CONCAT( X(".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key)."), ' ', Y(".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key).")) AS `" . $key."`");
                            break;

                        default:
                            array_push($select_clause, "`".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key)."`");
                        break;
                    }
                }
            }
        }
        else
        {
             foreach ($this->_data as $key => $value)
            {
                switch($this->_propertiesType[$key])
                {
                    case "utc_timestamp":
                        array_push($select_clause, "UNIX_TIMESTAMP(" . \Zeflasher\PhpDal\Db\Access::sanitiseQuery($key) . ") AS `" . $key."`");
                    break;

                    case "geom_point":
                        array_push($select_clause, "CONCAT( X(".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key)."), ' ', Y(".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key).")) AS `" . $key."`");
                    break;

                    default:
                        array_push($select_clause, "`".\Zeflasher\PhpDal\Db\Access::sanitiseQuery($key)."`");
                    break;
                }
            }
        }

        //  create the where clause
        $where_clause = array();
        foreach ($this->_data as $key => $value)
        {
            if (isset($this->{$key}))
            {
                array_push($where_clause, \Zeflasher\PhpDal\Db\Access::sanitiseQuery("`".$key . "` = '%s'", $value[self::INDEX_PROPERTY_LOCAL]));
            }
        }

        //  execute the query
        $sql = "SELECT " . join(", ", $select_clause) . " FROM `" . $this->getTableName() . "` WHERE " . join(" AND ", $where_clause);
        if (!empty($extra_clause))
        {
            $sql .= " " . $extra_clause;
        }
        $sql .= " LIMIT 1;";

        $sql_load = \Zeflasher\PhpDal\Db\Access::query($sql, $this->_database_type);
        if ($sql_load->num_rows != 1)
        {
            throw new \RuntimeException("DataPack::load failed: " . $sql);
        }

        //  push the mysql result into the DataObject
        //  we discard properties not set in the _data array
        $this->_load($sql_load);
    }

    /**
     * Execute any query on the specified table and database type.
     * Call like execute("Select * where id = %d", 1);
     * @param string $sql
     * @return bool|\MySQLi_Result
     */
    public function execute($sql = '')
    {
        //  execute the query on the db
        $sql = call_user_func_array( '\Zeflasher\PhpDal\Db\Access::sanitiseQuery', func_get_args() );
        return \Zeflasher\PhpDal\Db\Access::query($sql, $this->_database_type);
    }

    public function multi_execute($sql = '')
    {
        //  execute the query on the db
        $sql = call_user_func_array( '\Zeflasher\PhpDal\Db\Access::sanitiseQuery', func_get_args() );
        return \Zeflasher\PhpDal\Db\Access::multi_query($sql, $this->_database_type);
    }

    /**
     * Save the DataPack back to the DB.
     * This will push all properties marked as data and set to the DB.
     * @return bool|int false if no field has changed so insertion not needed or the newly created id on successful insertion.
     */
    //  TODO check if property all valid and send back the id.
    public function insert()
    {
        //  check all the required properties has been set
        $this->hasAllMandatoryInsertProperties();

        $sql = "INSERT INTO `". $this->getTableName() . "`";

        //  generate the sql for changed properties
        $sql_properties = $this->_generateSQLPropertyValue();
        if (!empty($sql_properties))
        {
            $sql .= " SET " . $sql_properties;
        }

        //  execute the insert query on the db
        $insert_id = \Zeflasher\PhpDal\Db\Access::queryInsertion($sql, $this->_database_type);

        $this->_validateAll();

        //  we deal with error in caller classes
        return $insert_id;
    }

    /**
     * Update the database
     * @param string $where_clause
     * @param int $limit
     * @return bool
     * @throws \RuntimeException
     */
    public function update($where_clause = '', $limit = 1)
    {
        //  generate the sql for changed properties
        $sql_properties = $this->_generateSQLPropertyValue();
        if (empty($sql_properties))
        {
            return false;
        }

        //  use primary key for update if no where clause passed
        if (empty($where_clause))
        {
            $this->hasAllPrimaryKeyProperties();
            $wheres_clause = array();
            foreach ($this->_primaryKeyProperties as $property)
            {
                if (isset($this->{$property}))
                {
                    array_push($wheres_clause, \Zeflasher\PhpDal\Db\Access::sanitiseQuery($property . " = '%s'", $this->_data[$property][self::INDEX_PROPERTY_LOCAL]));
                }
                else
                {
                    throw new \RuntimeException("All the primary keys need to be set on the object before calling the update method[$this->{$property}]");
                }
            }
            $where_clause = join(' AND ', $wheres_clause);
        }

        //  create and execute the update query on the db
        $sql = "UPDATE `" . $this->getTableName() . "` SET {$sql_properties} WHERE {$where_clause} LIMIT {$limit};";
        $update_query = \Zeflasher\PhpDal\Db\Access::query($sql, $this->_database_type);

        $this->_validateAll();

        return $update_query;
    }

    /**
     * Delete the record in the database
     * @param string $where_clause
     * @param int $limit
     * @return bool|\MySQLi_Result
     * @throws \RuntimeException
     */
    public function delete($where_clause = '', $limit = 1)
    {
        //  use primary key for update
        if (empty($where_clause))
        {
            $where_clauses = array();
            foreach ($this->_primaryKeyProperties as $property)
            {
                if (isset($this->{$property}))
                {
                    array_push($where_clauses, \Zeflasher\PhpDal\Db\Access::sanitiseQuery($property . " = '%s'", $this->_data[$property][self::INDEX_PROPERTY_LOCAL]));
                }
                else
                {
                    throw new \RuntimeException("All the primary keys need to be set on the object before calling the delete method");
                }
            }
            $where_clause = join(' AND', $where_clauses);
        }

        //  create and execute the delete query on the db
        $sql = "DELETE FROM `" . $this->getTableName() . "` WHERE {$where_clause} LIMIT {$limit};";
        $sql_delete = \Zeflasher\PhpDal\Db\Access::query($sql, $this->_database_type);
        return $sql_delete;
    }

    /* ****************************************************************************
    *	FINAL PROTECTED FUNCTIONS
    **************************************************************************** */

    /**
     * Generate the property = value, property = value, ... string for Insert and Update
     *
     * @return string
     */
    final protected function _generateSQLPropertyValue()
    {
        //  get all the changed properties (if any)
        $changedProperties = $this->getChangedProperties();

        //  if we have no changed properties return an object with code
        //  TODO define return code / message / debug, ...
        if (count($changedProperties) == 0)
        {
            return "";
        }

        //  create a sanitized property = value query
        $sql = "";
        foreach ($changedProperties as $key => $value)
        {
            if ($sql != "")
            {
                $sql .= ", ";
            }

            switch($this->_propertiesType[$key])
            {
                case "utc_timestamp":
                    $timestamp = date('Y-m-d H:i:s', $value[self::INDEX_PROPERTY_LOCAL]);
                    $sql .= \Zeflasher\PhpDal\Db\Access::sanitiseQuery("`".$key . "` = '%s'", $timestamp);
                break;

                case "geom_point":
                    $sql .= \Zeflasher\PhpDal\Db\Access::sanitiseQuery("`".$key . "` = GEOMFROMTEXT('POINT(%s)')", $value[self::INDEX_PROPERTY_LOCAL]);
                break;

                default:
                    $sql .= \Zeflasher\PhpDal\Db\Access::sanitiseQuery("`".$key . "` = '%s'", $value[self::INDEX_PROPERTY_LOCAL]);
                break;
            }
        }

        return $sql;
    }

    /**
     * Load in data to instance
     * Push the mysql result into the DataPack
     * We discard properties not set in the _data array
     *
     * @param \mysqli_result|array $object
     */
    final protected function _load($object)
    {
        // if passed a recordset loadRS, else loadAS
        if (is_object($object))
        {
            $this->_loadRS($object);
        }
        else
        {
            $this->_loadAS($object);
        }
    }

    /**
     * Parse recordset into this object
     *
     * @param \mysqli_result $rs
     */
    final protected function _loadRS($rs)
    {
        while ($row = $rs->fetch_assoc())
        {
            $this->_loadAS($row);
        }
    }

    /**
     * Parse array of properties in this object
     *
     * @param array $assocArray
     */
    final protected function _loadAS($assocArray = [])
    {
        foreach ($assocArray as $property => $value)
        {
            $this->_loadProperty($property, $value);
        }
    }

    /**/
}

?>
